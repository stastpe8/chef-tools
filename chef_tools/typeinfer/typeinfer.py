import logging
import re
from chef_testcase_parser import R_assignments_from_file, RTestCase

from dataclasses import dataclass
from pathlib import Path


@dataclass
class TypeInferAnnotation:
    state_id: int
    return_type: str
    parameters: list[str]


def get_acceptable_types(test_cases_path: Path) -> list[list[str]]:
    result: list[list[str]] = []
    test_cases: list[RTestCase] = R_assignments_from_file(test_cases_path)

    for test_case in test_cases:
        assignments = [_strip_vec_from_typename(assignment.get_type_name()) for assignment in test_case.assignments]
        if assignments not in result:
            result.append(assignments)

    return result


def generate_type_mapping(test_cases_path: Path, debug_log: Path) -> dict[tuple[str, ...], str]:
    """
    Return dict of types -> type
    """
    result: dict[tuple[str, ...], str] = {}
    annotations = _get_typeinfer_annotations(debug_log)
    test_cases = R_assignments_from_file(test_cases_path)

    for annotation in annotations:
        # Find the corresponding assignment based on state id.
        # There will always be at most one, as the testcases are generated
        # when a state is terminated
        test_case: RTestCase | None = next((t for t in test_cases if t.metadata.state_id == annotation.state_id), None)
        if test_case is None:
            logging.warning(f"Could not find assignment for state id {annotation.state_id}. Ignoring potential type mapping.")
            continue

        # Get types of the parameters
        parameter_names = annotation.parameters
        parameter_assignments = [
            next((assignment for assignment in test_case.assignments if assignment.name == name), None)
            for name in parameter_names
        ]
        if None in parameter_assignments:
            logging.warning(f"Found incomplete assignment for state id {annotation.state_id}. Ignoring potential type mapping.")
            continue

        types: list[str] = [
            _strip_vec_from_typename(assignment.get_type_name())  # type: ignore  # assignment cannot be None due to the previous check
            for assignment
            in parameter_assignments
        ]

        result[tuple(types)] = annotation.return_type

    return result



def _get_typeinfer_annotations(debug_log: Path) -> list[TypeInferAnnotation]:
    result: list[TypeInferAnnotation] = []

    for line in debug_log.open("r"):
        if "TYPEINFER" in line:
            match = re.search(r'\[State (\d+)]', line)
            if not match:
                raise ValueError("TYPEINFER message without state id")

            rhs = line.split("TYPEINFER")[1].strip()
            parts = rhs.split(" ")

            state_id = int(match.group(1))
            return_type = parts[0]
            parameters = parts[1:]

            result.append(TypeInferAnnotation(state_id, return_type, parameters))

    return result


def _strip_vec_from_typename(typename: str) -> str:
    """
    Types are often in format "vec<integer>" and so on.
    However, we are not interested in this information here. Every integer
    is a vector, so there is no need to distinguish between an integer (vec, len=1) and
    a vector of integers (vec, len>1).
    It just clutters the view and the user experience.
    """
    return re.sub(r"vec<(.+)>", r"\1", typename)