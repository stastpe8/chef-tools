from pathlib import Path

import click
from chef_tools.testgen import generate_testthat_script
from chef_tools.typeinfer import generate_type_mapping, get_acceptable_types


@click.group()
def main():
    pass


@main.command("testgen")
@click.argument("test_cases", type=click.Path(exists=True, path_type=Path), required=True)
@click.argument("source", type=click.Path(exists=True, path_type=Path), required=True)
def testgen(test_cases: Path, source: Path) -> None:
    """
    Generate test cases based of an execution.

    Reads test cases outputted by s2e and a source code of executed R script.
    Output a testthat-compatible testfile that can be used to test the R script.
    """
    print(generate_testthat_script(test_cases, source))


@main.command("typeinfer")
@click.argument("test_cases", type=click.Path(exists=True, path_type=Path), required=True)
@click.argument("debug_log", type=click.Path(exists=True, path_type=Path), required=False)
def typeinfer(test_cases: Path, debug_log: Path | None = None) -> None:
    """
    Generate type annotations of a function.

    This requires SUCCESSFUL test cases and optionally a debug log.
    In order for this tool to work, the user has to annotate
    the function execution. As an example:


    lhs <- chef.any(5)
    rhs <- chef.any(5)
    result <- lhs + rhs
    chef.debug(paste("TYPEINFER", typeof(result), "lhs", "rhs"))

    The TYPEINFER messages will be parsed out of debug log and used to
    track the function to infer and the return type.

    This will produce a mapping of input types to output type.

    When the debug log is not specified, the tool will only output list of accepted types, not the mapping itself.
    """
    if debug_log:
        type_mapping = generate_type_mapping(test_cases, debug_log)

        for key, value in type_mapping.items():
            key_identifier = "\t\t".join(key)
            print(f"{key_identifier}\t\t->\t{value}")
    else:
        types = get_acceptable_types(test_cases)

        for type in types:
            print("\t\t".join(type))
