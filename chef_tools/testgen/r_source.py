import re
from enum import Enum
from typing import Tuple


class VarType(str, Enum):
    Int = "int"
    String = "string"
    Vector = "vec"
    RawBytes = "raw"


def _remove_consecutive_blank_lines(lines: list[str]) -> list[str]:
    """
    We want to format R code. However, there is no R formatter available for Python.
    So here, we want to remove consecutive blank lines, as well as leading and trailing blank lines.

    So from this:
    ```
    [ '', '', 'a', '', '', 'b', '' ]
    ```
    We should get this:
    ```
    [ 'a', '', 'b' ]
    ```
    """
    new_lines = []
    prev_blank = True
    for line in lines:
        if line.strip():  # If line is not blank
            new_lines.append(line)
            prev_blank = False
        else:
            if not prev_blank:  # If previous line was not blank
                new_lines.append(line)
            prev_blank = True

    # If last line is empty, remove it
    if len(new_lines) >= 1 and new_lines[-1] == "":
        new_lines = new_lines[:-1]
    return new_lines


class RSource:
    def __init__(self, source_code: str):
        self.source: str = source_code
        self.source_lines: list[str] = source_code.split("\n")

    def import_lines(self) -> list[str]:
        """
        Get lines that contain a library() or require() call
        """
        return [
            line
            for line in self.source_lines
            if re.search("((library)|(require))\((.*)\)", line) is not None
        ]

    def source_stripped(self) -> list[str]:
        """
        Source code without library() and require() calls, most chef.* functions and
        with chef.assert replaced by expect_true()
        """

        # Replace chef.assert with expect_true and remove some functions that are no longer needed
        source_code = [
                line.replace("chef.assert", "expect_true")
                for line in self.source_lines
                if re.search("((library)|(require))\((.*)\)", line) is None
                and re.search("chef.(any|int|string|vec|raw|assume|start)\(.*\)", line)
                is None
            ]

        # Replace chef.end with call to expect_false (since T in chef.end is a failure)
        source_code = [
            line.replace("chef.end", "expect_false")
            for line in source_code
        ]

        # Replace chef.debug with print call
        source_code = [
            line.replace("chef.debug", "print")
            for line in source_code
        ]

        return _remove_consecutive_blank_lines(source_code)



    def chef_variables(self) -> list[Tuple[str, VarType]]:
        """
        Get all symbolic variables that are found in the source code.

        This captures all lines that contain [variable name] <- chef_[type](...)

        This returns both variable name and type, inferred from the type of chef function called.

        For example, this code:
        ```R
        a <- chef.int(10)
        b <- chef.string(15)
        ```
        would return:
        ```py
        [ ("a", VarType.Int), ("b", VarType.String) ]
        ```
        """
        return [
            (match.group(1), VarType(match.group(3)))
            for match in [
                re.search(
                    "([a-zA-Z0-9_]*) *(<-|=) *chef.(int|string|vec|raw)\(.*\)", line
                )
                for line in self.source_lines
            ]
            if match is not None
            and len(match.groups()) == 3
            and match.group(1) is not None
        ]
