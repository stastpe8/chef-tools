from pathlib import Path
from chef_testcase_parser import R_assignments_from_file

from chef_tools.testgen.r_source import RSource
from chef_tools.testgen.generator import generate_r_testthat


def generate_testthat_script(test_cases: Path, source: Path) -> str:
    R_assignments = R_assignments_from_file(test_cases)

    with source.open("r") as f:
        R_source = RSource(f.read())

    return generate_r_testthat(R_assignments, R_source)
