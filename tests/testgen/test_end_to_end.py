from pathlib import Path

import pytest

from click.testing import CliRunner

from chef_tools.cli import testgen


@pytest.mark.parametrize(
    "testcase",
    [
        "base64enc",
    ],
)
def test_verify_testgen(testcase: str):
    runner = CliRunner()

    current_dir = Path(__file__).parent
    root_dir = current_dir.parent.parent

    result = runner.invoke(testgen, [f"{root_dir}/examples/testgen/base64enc/err_test_cases.json", f"{root_dir}/examples/testgen/base64enc/source.R"])

    assert result.exit_code == 0

    with (current_dir / "data" / f"{testcase}.out").open("r") as f:
        assert result.output == f.read()
