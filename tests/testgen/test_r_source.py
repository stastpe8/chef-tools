import pytest

from chef_tools.testgen.r_source import _remove_consecutive_blank_lines, RSource, VarType


@pytest.mark.parametrize(
    "inlines,outlines",
    [
        ([], []),
        (["a", "", "b"], ["a", "", "b"]),
        (["a", "", "", "b"], ["a", "", "b"]),
        (["a", "", "", "b", "", ""], ["a", "", "b"]),
        (["", "a", "", "b", "", ""], ["a", "", "b"]),
    ],
)
def test_removing_consecutive_blank_lines(inlines, outlines):
    assert _remove_consecutive_blank_lines(inlines) == outlines


def test_variables():
    source = RSource(
        """
        foo <- chef.int()
        bar <- chef.string(42)
        baz <- chef.vec(18)
        foo2 <- chef.int("foo2")
        bar2 <- chef.raw(10)
        """
    )

    assert source.chef_variables() == [
        ("foo", VarType.Int),
        ("bar", VarType.String),
        ("baz", VarType.Vector),
        ("foo2", VarType.Int),
        ("bar2", VarType.RawBytes),
    ]
