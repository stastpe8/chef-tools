# Chef tools

This is part of the [Chef thesis](https://projects.fit.cvut.cz/theses/4754).

This can be used to process results from Chef.

```sh
pipx install chef-tools --index-url https://gitlab.fit.cvut.cz/api/v4/projects/60767/packages/pypi/simple
```

See the folder `examples` for sample data to run these tools on. Note that some of the files in the `example` folder are stored in git LFS.

## Testgen

Generates testthat-compatible testfiles out of (usually unsuccessful) test cases.

Requires test cases file and source code.

## Type infer

Infers probable types that a given function accepts, producing mapping from input to output types.

Requires successful test cases file and debug.txt in order to be able to produce full mapping.

Furthermore, the code has to be altered. After the function, the user has to use the `chef.debug` function
to print `TYPEINFER <typeOfResult> <namesOfArguments ...>`. For instance:

```R
chef.start()

tryCatch({
  lhs <- chef.any(5)
  rhs <- chef.any(5)
  result <- lhs + rhs
  # The typeinfer tool will know that the type of the result produced
  # was caused by lhs and rhs variables
  chef.debug(paste("TYPEINFER", typeof(result), "lhs", "rhs"))
}, error = function(e) {
  chef.end(TRUE)
})

chef.end(FALSE)
```

The type passed to `chef.debug` does NOT have to match a real R type -> so you can put information of any granularity
you want into it, and a corresponding mapping will be produced.
